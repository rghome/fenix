symfony3
========

A Symfony project created on October 4, 2016, 12:46 pm.


### Install
1. Install composer requirements:

   ```
   composer install
   ```
   
2. Create `parameters.yml` inside `app/config/`:

    ```
    parameters:
        database_host: 127.0.0.1
        database_port: ~
        database_name: symfony
        database_user: root
        database_password:
        mailer_transport: smtp
        mailer_host: 127.0.0.1
        mailer_user: null
        mailer_password: null
        secret: be21c26a47277965b564936f98cd9ccb6a8ef641

    ```

3. Create DataBase schema:

   ```
   php bin/console doctrine:schema:update --force
   ```
    
4. Create new Super User:

   ```
   php bin/console rm:user:create
   ```
   
   `username` and `password` must be `admin`
   
5. Add Oauth2 client

   ```
   INSERT INTO `oauth2_clients` VALUES (NULL, '3bcbxd9e24g0gk4swg0kwgcwg4o8k8g4g888kwc44gcc0gwwk4', 'a:0:{}', '4ok2x70rlfokc8g0wws8c8kwcokw80k44sg48goc0ok4w0so0k', 'a:1:{i:0;s:8:"password";}');
   ```
   
   The following step consists in adding a new OAuth2 client.