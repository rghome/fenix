<?php


namespace RM\CommonBundle\Handler;


use Symfony\Component\Form\FormFactory;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Request;
use RM\CommonBundle\Exception\ApiBadRequestException;
use RM\CommonBundle\ApiErrors;

class ApiFormHandler
{
    /**
     * @var FormFactory
     */
    protected $formFactory;

    /**
     * @var Request
     */
    protected $request;

    /**
     * @param FormFactory  $formFactory
     * @param RequestStack $requestStack
     */
    public function __construct(
        FormFactory $formFactory,
        RequestStack $requestStack
    ) {
        $this->formFactory = $formFactory;
        $this->request = $requestStack->getCurrentRequest();
    }

    /**
     *
     *
     * @param string $type
     * @param mixed   $data
     * @param array  $options
     *
     * @return Form|FormInterface
     */
    public function create($type = 'form', $data = null, array $options = [])
    {
        if ( ! isset($options['method'])) {
            $options['method'] = $this->request->getMethod();
        }

        return $this->formFactory->create($type, $data, $options);
    }

    /**
     *
     *
     * @param Form    $form
     * @param Request $request
     * @return Form   $form
     *
     * @throws ApiBadRequestException
     */
    public function handle(Form $form, Request $request = null)
    {
        $request = $request ? $request : $this->request;
        $form->handleRequest($request);

        if (!$form->isSubmitted()) {
            ApiBadRequestException::throwException(
                ApiErrors::FORM_NOT_SUBMITTED
            );
        }

        return $form;
    }
}