<?php


namespace RM\CommonBundle;


class ApiErrors
{
    const ACCESS_FORBIDDEN                                          = 1;

    // Form errors
    const FORM_VALIDATION_FAIL                                      = 1050;
    const FORM_NOT_SUBMITTED                                        = 1051;
    const FORM_EXTRA_FIELDS                                         = 1052;

    // filters errors
    const VALIDATION_FAIL                                           = 1500;
    const VALIDATION_COLLECTION_NOT_VALID                           = 1501;

    // User errors
    const USER_AUTH_WRONG_USERNAME_OR_PASSWORD                      = 1100;
    const USER_EMAIL_REQUIRED                                       = 1101;
    const USER_PASSWORD_PLAIN_REQUIRED                              = 1102;
    const USER_PASSWORD_PLAIN_CONFIRM_EMPTY                         = 1103;
    const USER_PASSWORDS_DO_NOT_MATCH                               = 1104;
    const USER_EMAIL_NOT_VALID                                      = 1105;
    const USER_NOT_FOUND                                            = 1106;
    const USER_PHONE_REQUIRED                                       = 1107;
    const USER_FIRST_NAME_REQUIRED                                  = 1108;
    const USER_LAST_NAME_REQUIRED                                   = 1109;
    const USER_PASSWORD_TO_SHORT                                    = 1110;
    const USER_IS_ACTIVE_NOT_VALID                                  = 1111;
    const USER_POINTS_NOT_VALID                                     = 1112;
    const USER_PHOTO_REQUIRED                                       = 1113;
    const USER_PHOTO_TO_LONG                                        = 1114;
    const USER_FIRST_NAME_TO_LONG                                   = 1115;
    const USER_LAST_NAME_TO_LONG                                    = 1116;

    // Track
    const TRACK_NOT_FOUND                                           = 1200;
    const TRACK_NAME_EMPTY                                          = 1201;
    const TRACK_NAME_TO_LONG                                        = 1202;

    /**
     * Messages for codes
     *
     * @var string[]
     */
    public static $messages = [
        self::ACCESS_FORBIDDEN => 'Access forbidden',
        self::VALIDATION_FAIL => 'Validation fail',
        self::FORM_VALIDATION_FAIL => 'Form validation fail',
        self::FORM_NOT_SUBMITTED => 'Form not submitted',
        self::FORM_EXTRA_FIELDS => 'This form should not contain extra fields',

        self::USER_AUTH_WRONG_USERNAME_OR_PASSWORD => 'Invalid email or password',
        self::USER_NOT_FOUND => 'User is not found',

//        self::USER_NOT_FOUND_BY_RESET_PASSWORD_TOKEN => 'User was not found  by reset password token',
//        self::USER_RESET_PASSWORD_TOKEN_WAS_EXPIRED => 'The reset password token was expired',
    ];

    /**
     * Get message by error code
     *
     * @param int $code
     * @return string $message
     */
    public static function getMessageByCode($code)
    {
        $message = isset(static::$messages[$code]) ? static::$messages[$code] : "Unknown error ({$code})";

        return $message;
    }
}