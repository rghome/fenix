<?php


namespace RM\CommonBundle\EventListener;


use JMS\Serializer\Serializer;

/**
 * Class RestListenerHelper
 * @package RM\CommonBundle\EventListener
 */
trait RestListenerHelper
{
    /**
     * @var Serializer $serializer
     */
    private $serializer;

    /**
     * @var array
     */
    private $data;

    /**
     * RestListenerHelper constructor.
     *
     * @param Serializer $serializer
     */
    public function __construct(Serializer $serializer)
    {
        $this->serializer = $serializer;

        $this->data = ['error' => [], 'result' => []];
    }

    /**
     * @param $error
     *
     * @return $this
     */
    private function setError($error)
    {
        $this->data['error'] = $error;

        return $this;
    }

    /**
     * @param $result
     *
     * @return $this
     */
    private function setResult($result)
    {
        $this->data['result'] = $result;

        return $this;
    }

    /**
     * @return array
     */
    private function getData()
    {
        return $this->data;
    }
}