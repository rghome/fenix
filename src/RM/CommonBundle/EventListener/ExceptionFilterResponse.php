<?php


namespace RM\CommonBundle\EventListener;


use RM\CommonBundle\Exception\ApiException;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class ExceptionFilterResponse
 * @package RM\CommonBundle\EventListener
 */
class ExceptionFilterResponse
{
    use RestListenerHelper;

    /**
     * @param GetResponseForExceptionEvent $event
     */
    public function onKernelResponse(GetResponseForExceptionEvent $event)
    {
        $exception =  $event->getException();

        $statusCode = 500;
        $messageArray = [
            'code' => 0,
            'message' => $exception->getMessage()
        ];

        if ($exception instanceof NotFoundHttpException) {
            $statusCode = $exception->getStatusCode();
            $messageArray['message'] = $exception->getMessage();

        } elseif ($exception instanceof BadRequestHttpException) {
            $statusCode = $exception->getStatusCode();
            $messageArray['message'] = $exception->getMessage();

        } elseif ($exception instanceof ConflictHttpException) {
            $statusCode = $exception->getStatusCode();
            $messageArray['message'] = $exception->getMessage();

        } elseif ($exception instanceof AccessDeniedHttpException) {
            $statusCode = $exception->getStatusCode();
            $messageArray['message'] = $exception->getMessage();

        } elseif ($exception instanceof ApiException) { // my custom exception with form object
            $statusCode = 400;
            if (false !== strpos($messageArray['message'], ApiException::MESSAGE_ERRORS_SEPARATOR)) {
                list ($message, $errors) = explode(ApiException::MESSAGE_ERRORS_SEPARATOR, $exception->getMessage(), 2);
                $messageArray = json_decode($errors, true);
            }
        } elseif ($exception instanceof \RuntimeException) {
            $statusCode = 400;
            $messageArray['message'] = $exception->getMessage();

        }

        $this->setError($messageArray);

        $content = $this->serializer->serialize($this->getData(), 'json');

        $response = new Response($content, $statusCode, ['ACCEPT' => 'application/json']);
        $event->setResponse($response);
    }
}