<?php


namespace RM\CommonBundle\EventListener;


use Symfony\Component\HttpKernel\Event\GetResponseForControllerResultEvent;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use JMS\Serializer\Serializer;

/**
 * Class ViewResponseListener
 * @package RM\CommonBundle\EventListener
 */
class ViewResponseListener
{
    use RestListenerHelper;

    /**
     * @param FilterResponseEvent $event
     *
     * @return bool
     */
    public function onKernelResponse(FilterResponseEvent $event)
    {
        $response = $event->getResponse();

        if ($event->getRequest()->attributes->get('_route') === 'nelmio_api_doc_index') {
            return true;
        }

        if ($response->isSuccessful()) {
            $this->setResult(json_decode($response->getContent(), true));

            $content = $this->serializer->serialize($this->getData(), 'json');

            $event->setResponse($response->setContent($content));
        }
    }
}