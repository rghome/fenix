<?php


namespace RM\CommonBundle\Exception;


/**
 * Class ApiBadRequestException
 * @package RM\CommonBundle\Exception
 */
class ApiBadRequestException extends ApiException { }