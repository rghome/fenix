<?php


namespace RM\CommonBundle\Exception;


/**
 * Class ApiUnprocessableEntityException
 * @package RM\CommonBundle\Exception
 */
class ApiUnprocessableEntityException extends ApiException { }