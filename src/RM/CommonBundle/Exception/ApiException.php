<?php


namespace RM\CommonBundle\Exception;


use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\Validation;
use \Exception;
use RM\CommonBundle\ApiErrors;

abstract class ApiException extends Exception
{
    const MESSAGE_ERRORS_SEPARATOR = ':message_errors_separator:';

    /**
     * @var array
     */
    protected $errors;

    /**
     * ApiException constructor.
     *
     * @param string $message
     * @param int $code
     * @param Exception|null $previous
     * @param array $errors
     */
    public function __construct($message, $code = 0, Exception $previous = null, array $errors = [])
    {
        $this->errors = $errors;

        if (count($errors)) {
            $message .= self::MESSAGE_ERRORS_SEPARATOR.json_encode($errors);
        }

        parent::__construct($message, $code, $previous);
    }

    /**
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * Throw Api exception
     *
     * @param int            $code
     * @param string         $message
     * @param array|ConstraintViolationListInterface $violations
     * @param Exception|null $previous
     *
     * @throws static
     */
    public static function throwException(
        $code = null,
        $message = null,
        $violations = null,
        Exception $previous = null
    ) {

        if (null === $message) {
            $message = ApiErrors::getMessageByCode($code);
        }

        $errors = [];
        if ($violations instanceof ConstraintViolationListInterface) {
            $errors = self::transformViolationsToArray($violations);
        } elseif (count($violations)) {
            $errors = $violations;
        }
fwrite(fopen('/tmp/dump', 'w'), print_r($errors, 1));
        throw new static(
            $message,
            $code,
            $previous,
            $errors
        );
    }

    /**
     * Validate value and throw an error if it's not valid
     *
     * @param mixed      $value
     * @param Constraint $constraint
     * @param string[]   $groups
     *
     * @throws static
     */
    public static function validate($value, Constraint $constraint, array $groups = null)
    {
        /**
         * for collection constraint there are bug when we cat get
         * wrong ref on constraint that contains error code from not
         * collection constraint
         *
         * https://github.com/symfony/symfony/issues/14639
         */

        $validator  = Validation::createValidator();
        $violations = $validator->validate($value, $constraint, $groups);

        $errors = self::transformViolationsToArray($violations);

        if ($errors) {
            static::throwException(ApiErrors::VALIDATION_FAIL, null, $errors);
        }
    }

    /**
     * @param ConstraintViolationListInterface $violations
     *
     * @return array
     */
    protected static function transformViolationsToArray(ConstraintViolationListInterface $violations)
    {
        $errors = [];
        foreach ($violations as $violation) {
            $constraint = $violation->getConstraint();

            $code = $constraint->payload['code'];
            if (is_string($code)) {
                $code = constant(ApiErrors::class.'::'.$code);
            }

            $errors[] = [
                'code'    => $code,
                'message' => $violation->getMessage(),
            ];
        }

        return $errors;
    }
}