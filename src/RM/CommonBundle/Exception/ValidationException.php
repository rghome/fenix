<?php


namespace RM\CommonBundle\Exception;


use Symfony\Component\Validator\ConstraintViolationListInterface;

/**
 * Class ValidationException
 * @package RM\CommonBundle\Exception
 */
class ValidationException extends \Exception
{
    /**
     * @var ConstraintViolationListInterface
     */
    protected $violations;


    /**
     * @param ConstraintViolationListInterface $violations
     */
    public function __construct(ConstraintViolationListInterface $violations)
    {
        $this->violations = $violations;
    }

    /**
     * @return ConstraintViolationListInterface
     */
    public function getViolations()
    {
        return $this->violations;
    }
}