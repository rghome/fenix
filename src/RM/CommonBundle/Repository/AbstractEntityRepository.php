<?php


namespace RM\CommonBundle\Repository;


use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;

/**
 * Class AbstractEntityRepository
 * @package RM\CommonBundle\Repository
 */
abstract class AbstractEntityRepository extends EntityRepository
{
    /**
     * @param object $entity
     * @param bool $flush
     */
    public function save($entity, $flush = true)
    {
        $em = $this->getEntityManager();

        $em->persist($entity);

        if ($flush) {
            $em->flush();
        }
    }

    /**
     * @param object $entity
     * @param bool $flush
     */
    public function remove($entity, $flush = true)
    {
        $em = $this->getEntityManager();

        $em->remove($entity);

        if ($flush) {
            $em->flush();
        }
    }

    /**
     * Getting the Entity manager
     *
     * @return EntityManager
     */
    public function getEm()
    {
        return $this->_em;
    }

    /**
     * Getting the Class Name
     *
     * @return string
     */
    public function getEntityClass()
    {
        return $this->_entityName;
    }
}