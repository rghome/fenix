<?php


namespace RM\CommonBundle\Form\Transformer;


use Symfony\Component\Form\DataTransformerInterface;

/**
 * Class ToStringTransformer
 * @package RM\CommonBundle\Form\Transformer
 */
class ToStringTransformer implements DataTransformerInterface
{
    /**
     * @param mixed $value
     *
     * @return string
     */
    public function transform($value)
    {
        if (is_array($value)) {
            return '';
        }

        return (string) $value;
    }

    /**
     * @param mixed $value
     *
     * @return string
     */
    public function reverseTransform($value)
    {
        if (is_array($value)) {
            return '';
        }

        return (string) $value;
    }
}