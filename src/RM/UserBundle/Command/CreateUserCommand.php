<?php


namespace RM\UserBundle\Command;


use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;

class CreateUserCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('rm:user:create')
            ->setDescription('Create a user.')
            ->setDefinition(array(
                new InputArgument('username', InputArgument::REQUIRED, 'The username'),
                new InputArgument('firstName', InputArgument::REQUIRED, 'The first name'),
                new InputArgument('lastName', InputArgument::REQUIRED, 'The last name'),
                new InputArgument('phone', InputArgument::REQUIRED, 'The phone number'),
                new InputArgument('email', InputArgument::REQUIRED, 'The email'),
                new InputArgument('password', InputArgument::REQUIRED, 'The password'),
                new InputOption('super-admin', null, InputOption::VALUE_NONE, 'Set the user as super admin'),
                new InputOption('inactive', null, InputOption::VALUE_NONE, 'Set the user as inactive'),
            ))
            ->setHelp(<<<EOT
The <info>rm:user:create</info> command creates a user:

  <info>php %command.full_name% matthieu</info>

This interactive shell will ask you for an email and then a password.

You can alternatively specify the email and password as the second and third arguments:

  <info>php %command.full_name% matthieu matthieu@example.com mypassword</info>

You can create a super admin via the super-admin flag:

  <info>php %command.full_name% admin --super-admin</info>

You can create an inactive user (will not be able to log in):

  <info>php %command.full_name% thibault --inactive</info>

EOT
            );
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $username   = $input->getArgument('username');
        $firstName  = $input->getArgument('firstName');
        $lastName   = $input->getArgument('lastName');
        $phone      = $input->getArgument('phone');
        $email      = $input->getArgument('email');
        $password   = $input->getArgument('password');
        $inactive   = $input->getOption('inactive');
        $superadmin = $input->getOption('super-admin');

        $manipulator = $this->getContainer()->get('rm_user.util.user_manipulator');
        $manipulator->setFirstName($firstName);
        $manipulator->setLastName($lastName);
        $manipulator->setPhone($phone);
        $manipulator->create($username, $password, $email, !$inactive, $superadmin);

        $output->writeln(sprintf('Created user <comment>%s</comment>', $username));
    }

    /**
     * {@inheritdoc}
     */
    protected function interact(InputInterface $input, OutputInterface $output)
    {
        if (!$this->getHelperSet()->has('question')) {
            $this->legacyInteract($input, $output);

            return;
        }

        $questions = array();

        if (!$input->getArgument('username')) {
            $question = new Question('Please choose a username:');
            $question->setValidator(function($username) {
                if (empty($username)) {
                    throw new \Exception('Username can not be empty');
                }

                return $username;
            });
            $questions['username'] = $question;
        }

        if (!$input->getArgument('firstName')) {
            $question = new Question('Please choose a first Name:');
            $question->setValidator(function($firstName) {
                if (empty($firstName)) {
                    throw new \Exception('First Name can not be empty');
                }

                return $firstName;
            });
            $questions['firstName'] = $question;
        }

        if (!$input->getArgument('lastName')) {
            $question = new Question('Please choose a last Name:');
            $question->setValidator(function($lastName) {
                if (empty($lastName)) {
                    throw new \Exception('Last Name can not be empty');
                }

                return $lastName;
            });
            $questions['lastName'] = $question;
        }

        if (!$input->getArgument('phone')) {
            $question = new Question('Please choose a phone:');
            $question->setValidator(function($phone) {
                if (empty($phone)) {
                    throw new \Exception('Phone can not be empty');
                }

                return $phone;
            });
            $questions['phone'] = $question;
        }

        if (!$input->getArgument('email')) {
            $question = new Question('Please choose an email:');
            $question->setValidator(function($email) {
                if (empty($email)) {
                    throw new \Exception('Email can not be empty');
                }

                return $email;
            });
            $questions['email'] = $question;
        }

        if (!$input->getArgument('password')) {
            $question = new Question('Please choose a password:');
            $question->setValidator(function($password) {
                if (empty($password)) {
                    throw new \Exception('Password can not be empty');
                }

                return $password;
            });
            $question->setHidden(true);
            $questions['password'] = $question;
        }

        foreach ($questions as $name => $question) {
            $answer = $this->getHelper('question')->ask($input, $output, $question);
            $input->setArgument($name, $answer);
        }
    }

    // BC for SF <2.5
    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     */
    private function legacyInteract(InputInterface $input, OutputInterface $output)
    {
        if (!$input->getArgument('username')) {
            $username = $this->getHelper('dialog')->askAndValidate(
                $output,
                'Please choose a username:',
                function($username) {
                    if (empty($username)) {
                        throw new \Exception('Username can not be empty');
                    }

                    return $username;
                }
            );
            $input->setArgument('username', $username);
        }

        if (!$input->getArgument('firstName')) {
            $firstName = $this->getHelper('dialog')->askAndValidate(
                $output,
                'Please choose a first Name:',
                function($firstName) {
                    if (empty($firstName)) {
                        throw new \Exception('firstName can not be empty');
                    }

                    return $firstName;
                }
            );
            $input->setArgument('firstName', $firstName);
        }

        if (!$input->getArgument('lastName')) {
            $lastName = $this->getHelper('dialog')->askAndValidate(
                $output,
                'Please choose a last Name:',
                function($lastName) {
                    if (empty($lastName)) {
                        throw new \Exception('lastName can not be empty');
                    }

                    return $lastName;
                }
            );
            $input->setArgument('lastName', $lastName);
        }

        if (!$input->getArgument('phone')) {
            $phone = $this->getHelper('dialog')->askAndValidate(
                $output,
                'Please choose a phone:',
                function($phone) {
                    if (empty($phone)) {
                        throw new \Exception('Phone can not be empty');
                    }

                    return $phone;
                }
            );
            $input->setArgument('phone', $phone);
        }

        if (!$input->getArgument('email')) {
            $email = $this->getHelper('dialog')->askAndValidate(
                $output,
                'Please choose an email:',
                function($email) {
                    if (empty($email)) {
                        throw new \Exception('Email can not be empty');
                    }

                    return $email;
                }
            );
            $input->setArgument('email', $email);
        }

        if (!$input->getArgument('password')) {
            $password = $this->getHelper('dialog')->askHiddenResponseAndValidate(
                $output,
                'Please choose a password:',
                function($password) {
                    if (empty($password)) {
                        throw new \Exception('Password can not be empty');
                    }

                    return $password;
                }
            );
            $input->setArgument('password', $password);
        }
    }
}