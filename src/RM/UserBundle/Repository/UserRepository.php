<?php


namespace RM\UserBundle\Repository;


use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\Query;
use RM\CommonBundle\Repository\AbstractEntityRepository;

/**
 * Class UserRepository
 * @package RM\UserBundle\Repository
 */
class UserRepository extends AbstractEntityRepository
{
    /**
     * @param $id
     * @param int $hydrationMode
     *
     * @return mixed
     */
    public function findProfileForApi($id, $hydrationMode = Query::HYDRATE_OBJECT)
    {
        return $this->createQueryBuilder('user')
            ->where('user.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult($hydrationMode);
    }
}