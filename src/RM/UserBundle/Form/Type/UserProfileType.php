<?php


namespace RM\UserBundle\Form\Type;


use RM\CommonBundle\Form\Transformer\ToStringTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use RM\UserBundle\Entity\User;


/**
 * Class UserProfileType
 * @package RM\UserSiteBundle\Form\Type
 */
class UserProfileType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $toStringTransformer = new ToStringTransformer();

        $builder
            ->add(
                $builder
                    ->create('firstName', TextType::class)
                    ->addModelTransformer($toStringTransformer)
            )
            ->add(
                $builder
                    ->create('lastName', TextType::class)
                    ->addModelTransformer($toStringTransformer)
            )
            ->add(
                $builder
                    ->create('email', TextType::class)
                    ->addModelTransformer($toStringTransformer)
            )
            ->add(
                $builder
                    ->create('photo', TextType::class)
                    ->addModelTransformer($toStringTransformer)
            )
            ->add(
                $builder
                    ->create('phone', TextType::class)
                    ->addModelTransformer($toStringTransformer)
            )
            ->add(
                $builder
                    ->create('location', TextType::class)
                    ->addModelTransformer($toStringTransformer)
            )
            ->add(
                $builder
                    ->create('photo', TextType::class)
                    ->addModelTransformer($toStringTransformer)
            )
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}