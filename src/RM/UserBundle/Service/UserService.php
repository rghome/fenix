<?php


namespace RM\UserBundle\Service;


use RM\CommonBundle\Repository\AbstractEntityRepository;
use RM\CommonBundle\Service\AbstractEntityService;
use RM\CommonBundle\Exception\ValidationException;
use RM\UserBundle\Entity\User;
use RM\UserBundle\Repository\UserRepository;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * Class UserService
 * @package RM\CommonBundle\Service
 */
class UserService extends AbstractEntityService
{
    /**
     * @var UserRepository
     */
    protected $userRepository;

    /**
     * @var ValidatorInterface
     */
    protected $validator;

    /**
     * @var UserPasswordEncoderInterface
     */
    protected $encoder;

    /**
     * UserService constructor.
     *
     * @param UserRepository $userRepository
     * @param ValidatorInterface $validator
     * @param UserPasswordEncoderInterface $encoder
     */
    public function __construct(
        UserRepository $userRepository,
        ValidatorInterface $validator,
        UserPasswordEncoderInterface $encoder
    )
    {
        $this->userRepository = $userRepository;
        $this->validator = $validator;
        $this->encoder = $encoder;
    }

    /**
     * @return UserRepository
     */
    public function getRepository()
    {
        return $this->userRepository;
    }

    /**
     * @param User $user
     *
     * @throws ValidationException
     */
    public function update(User $user)
    {
        $violations = $this->validator->validate($user, null, 'user_profile');

        if (count($violations)) {
            throw new ValidationException($violations);
        }

        if ($user->getPlainPassword()) {
            $passwordHash = $this->encoder->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($passwordHash);
        }

        $this->userRepository->save($user);
    }
}