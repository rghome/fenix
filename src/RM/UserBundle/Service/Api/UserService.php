<?php


namespace RM\UserBundle\Service\Api;

use RM\UserBundle\Entity\User;
use RM\UserBundle\Repository\UserRepository;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Doctrine\ORM\Query;

/**
 * Class UserService
 * @package RM\UserBundle\Service\Api
 */
class UserService
{
    /**
     * @var TokenStorage
     */
    protected $tokenStorage;


    /**
     * @var UserRepository
     */
    protected $userRepository;

    /**
     * UserService constructor.
     *
     * @param TokenStorage $tokenStorage
     * @param UserRepository $userRepository
     */
    public function __construct(
        TokenStorage $tokenStorage,
        UserRepository $userRepository
    )
    {
        $this->tokenStorage = $tokenStorage;
        $this->userRepository = $userRepository;
    }

    /**
     * @param $id
     *
     * @return mixed
     */
    public function get($id)
    {
        return $this->userRepository->findProfileForApi($id);
    }

    /**
     * Return authorised user information
     *
     * @return User|null
     */
    public function getAuthorisedUser() : User
    {
        $token = $this->tokenStorage ? $this->tokenStorage->getToken() : null;
        $user = $token ? $token->getUser() : null;

        return $user;
    }

}