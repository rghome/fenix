<?php


namespace RM\UserBundle\Entity;


use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use RM\MapBundle\Entity\Track;

/**
 * Class User
 *
 * @ORM\Entity
 * @ORM\Table("users")
 * @ORM\Entity(repositoryClass="RM\UserBundle\Repository\UserRepository")
 *
 * @package RM\UserBundle\Entity
 */
class User extends BaseUser
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(
     *     type="string",
     *     length=255,
     *     nullable=false,
     *     options={"default"=false}
     * )
     */
    protected $firstName;

    /**
     * @ORM\Column(
     *     type="string",
     *     length=255,
     *     nullable=false,
     *     options={"default"=false}
     * )
     */
    protected $lastName;

    /**
     * @ORM\Column(
     *     type="string",
     *     length=255,
     *     nullable=true
     * )
     */
    protected $photo;

    /**
     * @ORM\Column(
     *     type="string",
     *     length=255,
     *     nullable=true
     * )
     */
    protected $location;

    /**
     * @ORM\Column(
     *     type="string",
     *     length=255,
     *     nullable=false,
     *     options={"default"=false}
     * )
     */
    protected $phone;

    /**
     * @ORM\OneToMany(
     *     targetEntity="RM\MapBundle\Entity\Track",
     *     mappedBy="user",
     *     cascade={"all"},
     *     orphanRemoval=true
     * )
     */
    protected $track;

    /**
     * User constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->track = new ArrayCollection();
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     *
     * @return User
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     *
     * @return User
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set photo
     *
     * @param string $photo
     *
     * @return User
     */
    public function setPhoto($photo)
    {
        $this->photo = $photo;

        return $this;
    }

    /**
     * Get photo
     *
     * @return string
     */
    public function getPhoto()
    {
        return $this->photo;
    }

    /**
     * Set location
     *
     * @param string $location
     *
     * @return User
     */
    public function setLocation($location)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location
     *
     * @return string
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return User
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Add track
     *
     * @param \RM\MapBundle\Entity\Track $track
     *
     * @return User
     */
    public function addTrack(\RM\MapBundle\Entity\Track $track)
    {
        $this->track[] = $track;

        return $this;
    }

    /**
     * Remove track
     *
     * @param \RM\MapBundle\Entity\Track $track
     */
    public function removeTrack(\RM\MapBundle\Entity\Track $track)
    {
        $this->track->removeElement($track);
    }

    /**
     * Get track
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTrack()
    {
        return $this->track;
    }
}
