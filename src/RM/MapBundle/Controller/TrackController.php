<?php


namespace RM\MapBundle\Controller;


use RM\CommonBundle\Exception\ValidationException;
use RM\CommonBundle\Handler\ApiFormHandler;
use RM\MapBundle\Entity\Track;
use RM\MapBundle\Service\TrackService;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\View;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use RM\UserBundle\Service\Api\UserService as ApiUserService;
use RM\MapBundle\Form\Type\TrackType;
use RM\CommonBundle\Exception\ApiUnprocessableEntityException;
use RM\CommonBundle\ApiErrors;

/**
 * Class TrackController
 * @package RM\MapBundle\Controller
 */
class TrackController
{
    /**
     * @var ApiFormHandler
     */
    protected $apiFormHandler;

    /**
     * @var ApiUserService
     */
    protected $apiUserService;

    /**
     * @var TrackService
     */
    protected $trackService;

    /**
     * TrackController constructor.
     *
     * @param ApiUserService $apiUserService
     * @param ApiFormHandler $apiFormHandler
     * @param TrackService $trackService
     */
    public function __construct(
        ApiUserService $apiUserService,
        ApiFormHandler $apiFormHandler,
        TrackService $trackService
    )
    {
        $this->apiUserService = $apiUserService;
        $this->apiFormHandler = $apiFormHandler;
        $this->trackService = $trackService;
    }

    /**
     * Create new track
     *
     * @ApiDoc(
     *     authentication=true,
     *     description="Create new track",
     *     section="Track",
     *     input="RM\MapBundle\Form\Type\TrackType"
     * )
     *
     * @Post(
     *     "/track",
     *     name="_create_track"
     * )
     *
     * @View()
     *
     * @param Request $request
     */
    public function createAction(Request $request)
    {
        $authorisedUser = $this->apiUserService->getAuthorisedUser();

        /** @var Track $track */
        $track = $this->trackService->createNewEntity();
        $form = $this->apiFormHandler->create(TrackType::class, $track);
        $this->apiFormHandler->handle($form, $request);

        try {
            $this->trackService->create($track, $authorisedUser);
        } catch (ValidationException $e) {
            ApiUnprocessableEntityException::throwException(
                ApiErrors::FORM_VALIDATION_FAIL, null, $e->getViolations()
            );
        }
    }
}