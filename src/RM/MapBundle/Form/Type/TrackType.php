<?php


namespace RM\MapBundle\Form\Type;


use RM\CommonBundle\Form\Transformer\ToStringTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use RM\MapBundle\Entity\Track;

/**
 * Class TrackType
 * @package RM\MapBundle\Form\Type
 */
class TrackType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $toStringTransformer = new ToStringTransformer();

        $builder
            ->add(
                $builder
                    ->create('name', TextType::class)
                    ->addModelTransformer($toStringTransformer)
            )
            ->add(
                $builder
                    ->create('isActive', CheckboxType::class)
            )
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Track::class
        ]);
    }
}