<?php


namespace RM\MapBundle\Service;


use RM\CommonBundle\Service\AbstractEntityService;
use RM\MapBundle\Entity\Track;
use RM\MapBundle\Repository\TrackRepository;
use RM\UserBundle\Entity\User;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use RM\CommonBundle\Exception\ValidationException;

/**
 * Class TrackService
 * @package RM\MapBundle\Service
 */
class TrackService extends AbstractEntityService
{
    /**
     * @var TrackRepository
     */
    protected $trackRepository;

    /**
     * @var ValidatorInterface
     */
    protected $validator;

    /**
     * TrackService constructor.
     *
     * @param TrackRepository $trackRepository
     * @param ValidatorInterface $validator
     */
    public function __construct(
        TrackRepository $trackRepository,
        ValidatorInterface $validator
    )
    {
        $this->trackRepository = $trackRepository;
        $this->validator = $validator;
    }

    /**
     * @return TrackRepository
     */
    public function getRepository()
    {
        return $this->trackRepository;
    }

    public function create(Track $track, User $user)
    {
        $violations = $this->validator->validate($track, null, 'track_create');

        if (count($violations)) {
            throw new ValidationException($violations);
        }
    }
}