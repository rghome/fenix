<?php


namespace RM\MapBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use RM\UserBundle\Entity\User;

/**
 * Class Track
 *
 * @ORM\Entity
 * @ORM\Table("track")
 * @ORM\Entity(repositoryClass="RM\MapBundle\Repository\TrackRepository")
 *
 * @package RM\MapBundle\Entity
 */
class Track
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(
     *     type="string",
     *     length=255,
     *     nullable=false,
     *     options={"default"=false}
     * )
     */
    protected $name;

    /**
     * @var bool
     *
     * @ORM\Column(
     *     type="boolean",
     *     options={"default"=0}
     * )
     */
    protected $isActive;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(
     *     type="datetime"
     * )
     */
    protected $created;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(
     *     type="datetime"
     * )
     */
    protected $updated;

    /**
     * @var User
     *
     * @ORM\ManyToOne(
     *     targetEntity="RM\UserBundle\Entity\User",
     *     inversedBy="track"
     * )
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Track
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     *
     * @return Track
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return Track
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     *
     * @return Track
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set user
     *
     * @param \RM\UserBundle\Entity\User $user
     *
     * @return Track
     */
    public function setUser(\RM\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \RM\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
}
