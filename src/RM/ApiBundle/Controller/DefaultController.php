<?php

namespace RM\ApiBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\View;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;


class DefaultController extends FOSRestController
{
    /**
     * Load Index Action
     *
     * @ApiDoc(
     *     authentication=true,
     *     description="Index Action",
     *     section="Main",
     * )
     *
     * @Get(
     *     "/",
     *     name="_index"
     * )
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();

        $view = $this->view([
            'user' => $user
        ]);

        return $this->handleView($view);
    }
}
