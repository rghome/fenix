<?php


namespace RM\ApiBundle\Controller;


use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\View;
//use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\Annotations\Put;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use RM\CommonBundle\ApiErrors;
use RM\CommonBundle\Exception\ValidationException;
use RM\CommonBundle\Exception\ApiUnprocessableEntityException;
use RM\CommonBundle\Handler\ApiFormHandler;
use RM\UserBundle\Entity\User;
use RM\UserBundle\Service\Api\UserService as ApiUserService;
use RM\UserBundle\Service\UserService;
use Symfony\Component\HttpFoundation\Request;
use RM\UserBundle\Form\Type\UserProfileType;

/**
 * Class ProfileController
 * @package RM\ApiBundle\Controller
 */
class ProfileController extends FOSRestController
{
    /**
     * @var ApiUserService
     */
    protected $apiUserService;

    /**
     * @var UserService
     */
    protected $userService;

    /**
     * @var ApiFormHandler
     */
    protected $apiFormHandler;

    /**
     * ProfileController constructor.
     *
     * @param ApiUserService $apiUserService
     * @param UserService $userService
     * @param ApiFormHandler $apiFormHandler
     */
    public function __construct(
        ApiUserService $apiUserService,
        UserService $userService,
        ApiFormHandler $apiFormHandler
    )
    {
        $this->apiUserService = $apiUserService;
        $this->userService = $userService;
        $this->apiFormHandler = $apiFormHandler;
    }

    /**
     * User profile information
     *
     * ### Response (200) ###
     * ```
     * Serialized User Entity
     * ```
     *
     * @ApiDoc(
     *     authentication=true,
     *     description="User profile information",
     *     section="Profile"
     * )
     *
     * @Get(
     *     "/user/profile",
     *     name="_profile"
     * )
     *
     * @View()
     *
     * @return User
     */
    public function currentAction()
    {
        return $this->apiUserService->getAuthorisedUser();
    }

    /**
     * Update user profile
     *
     * ### Response (200) ###
     * ```
     *  Look at GET /user/profile
     * ```
     *
     * @ApiDoc(
     *     authentication=true,
     *     description="User profile information",
     *     section="Profile",
     *     input="RM\UserBundle\Form\Type\UserProfileType"
     * )
     *
     * @Put(
     *     "/user/profile",
     *     name="_profile"
     * )
     *
     * @View()
     *
     * @param Request $request
     * @return User
     */
    public function updateAction(Request $request)
    {
        $authorisedUser = $this->apiUserService->getAuthorisedUser();

        $form = $this->apiFormHandler->create(UserProfileType::class, $authorisedUser);
        $this->apiFormHandler->handle($form, $request);

        try {
            $this->userService->update($authorisedUser);
        } catch (ValidationException $e) {
            ApiUnprocessableEntityException::throwException(
                ApiErrors::FORM_VALIDATION_FAIL, null, $e->getViolations()
            );
        }

        return $this->apiUserService->get($authorisedUser->getId());
    }
}